'use strict'
const Ws = use('Ws')
const Config = use('Config')

class PruebaController {
  test() {
    const topic = Ws.getChannel('guides').topic('guides')
    if(topic){
      topic.broadcastToAll('message', 'ACA LA NUEVA GUÍA')
    }
  }
}

module.exports = PruebaController
